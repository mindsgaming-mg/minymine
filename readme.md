## API  for MTCG

https://mindsgaming.glitch.me/mtcg

(A Game Miner for MintMe with CoinImp Base)


## How To USE

1.) [Remix](https://glitch.com/edit/#!/remix/minymine) This project on Glitch

2.) Edit the /miner index.html with your script from [CoinIMP.com](https://www.coinimp.com/invite/7379384b-1c8a-4a04-b415-0b0892454835)

3.) Invite users to your site to mine Mintme for you!